#include <Key.h>

#include <Keypad.h>

#define PIN 10        // pin pro led
#define NUM_LEDS 49   // number of diods

#include "Adafruit_NeoPixel.h"

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);

const byte ROWS = 7; //seven rows
const byte COLS = 7; //seven columns

//define the cymbols on the buttons of the keypads
char keys[ROWS][COLS] = {
  {'0','D','E','R','S','f','g'},
  {'1','C','F','Q','T','e','h'},
  {'2','B','G','P','U','d','i'},
  {'3','A','H','O','V','c','j'},
  {'4','9','I','N','W','b','k'},
  {'5','8','J','M','X','a','l'},
  {'6','7','K','L','Y','Z','m'}
};
byte rowPins[ROWS] = {9, A0, A1, A2, A3, 14, 15}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {2, 3, 4, 5, 6, 7, 8};       //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad launchpad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

// Vector for start animation main diagonal
int animMD[13][7] = {
  { 0, 0, 0, 0, 0, 0, 0},
  { 1, 13, 0, 0, 0, 0, 0},
  { 2, 12, 14, 0, 0, 0, 0},
  { 3, 11, 15, 27, 0, 0, 0},
  { 4, 10, 16, 26, 28, 0, 0},
  { 5, 9, 17, 25, 29, 41, 0},
  { 6, 8, 18, 24, 30, 40, 42},
  { 7, 19, 23, 31, 39, 43, 0},
  { 20, 22, 32, 38, 44, 0, 0},
  { 21, 33, 37, 45, 0, 0, 0},
  { 34, 36, 46, 0, 0, 0, 0},
  { 35, 47, 0, 0, 0, 0, 0},
  { 48, 0, 0, 0, 0, 0, 0}
};

// Vector for start animation sub diagonal
int animSD[13][7] = {
  { 6, 0, 0, 0, 0, 0, 0},
  { 5, 7, 0, 0, 0, 0, 0},
  { 4, 8, 20, 0, 0, 0, 0},
  { 3, 9, 19, 21, 0, 0, 0},
  { 2, 10, 18, 22, 34, 0, 0},
  { 1, 11, 17, 23, 33, 35, 0},
  { 0, 12, 16, 24, 32, 36, 48},
  { 13, 15, 25, 31, 37, 47, 0},
  { 14, 26, 30, 38, 46, 0, 0},
  { 27, 29, 39, 45, 0, 0, 0},
  { 28, 40, 44, 0, 0, 0, 0},
  { 41, 43, 0, 0, 0, 0, 0},
  { 42, 0, 0, 0, 0, 0, 0}
};

// Animation for white rhytm diods
int rhytm[16] = { 3, 10, 17, 24, 21, 22, 23, 24, 45, 38, 31, 24, 27, 26, 25, 24 };

// Vectors for animation each of four small pads 3x3
int padLU[9] = { 0, 1, 2, 11, 16, 15, 14, 13, 12 };
int padRU[9] = { 4, 5, 6, 7, 20, 19, 18, 9, 8 };
int padLD[9] = { 28, 29, 30, 39, 44, 43, 42, 41, 40 };
int padRD[9] = { 32, 33, 34, 35, 48, 47, 46, 37, 36 };

void setup(){
  Serial.begin(9600);

  // Begin settings for led strip
  strip.begin();
  strip.setBrightness(50);    // яркость, от 0 до 255
  strip.clear();                          // очистить
  strip.show();   


  // Start Animation
  for (int i = 0; i < 43; i++ ) {   // от 0 до первой трети
    switch(i) {
      case 0:
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000);   
      break;
     
      case 1:
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000);   
      break;
        
      case 2:
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000);   
      break;

      case 3:
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000);   
      break;

      case 4:
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000);   
      break;

      case 5:
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000);   
      break;

      case 6:
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);
        for (int j = 0; j < 7; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000);   
      break;

      case 7:
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000);    
      break;

      case 8:
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000); 
      break;

      case 9:
        for (int j = 0; j < 7; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000);
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00); 
      break;

      case 10:
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000); 
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00);  
      break;

      case 11:
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);  
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000);  
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00);
      break;

      case 12:
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animMD[i][j], 0xff0000);
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00); 
      break;

      case 13:
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00);  
      break;

      case 14:
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00);
      break;

      case 15:
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animMD[i-3][j], 0x000000);
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 7; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00);
      break;

      case 16:
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00);
      break;

      case 17:
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00);
      break;

      case 18:
        for (int j = 0; j < 7; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00);
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animMD[i-6][j], 0xffff00);
      break;

      case 19:
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00);
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animMD[i-8][j], 0xffff00);
      break;

      case 20:
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00);
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animMD[i-10][j], 0xffff00);
      break;

      case 21:
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animMD[i-9][j], 0x000000);
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animSD[i-9][j], 0x00ff00);
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animMD[i-12][j], 0xffff00);
      break;

      case 22:
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animMD[i-11][j], 0x000000);
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animMD[i-14][j], 0xffff00);
      break;

      case 23:
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animMD[i-13][j], 0x000000);
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animMD[i-16][j], 0xffff00);
      break;

      case 24:
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animSD[i-12][j], 0x000000);
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animMD[i-15][j], 0x000000);
        for (int j = 0; j < 7; j++ ) strip.setPixelColor(animMD[i-18][j], 0xffff00);
      break;

      case 25:
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animMD[i-17][j], 0x000000);
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animMD[i-20][j], 0xffff00);
      break;

      case 26:
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animMD[i-19][j], 0x000000);
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animMD[i-22][j], 0xffff00);
      break;

      case 27:
        for (int j = 0; j < 7; j++ ) strip.setPixelColor(animMD[i-21][j], 0x000000);
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animMD[i-24][j], 0xffff00);

        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animSD[i-15][j], 0x0000ff);
      break;

      case 28:
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animMD[i-23][j], 0x000000);
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animMD[i-26][j], 0xffff00);

        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animSD[i-17][j], 0x0000ff);
      break;

      case 29:
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animMD[i-25][j], 0x000000);
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animMD[i-28][j], 0xffff00);

        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animSD[i-19][j], 0x0000ff);
      break;

      case 30:
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animMD[i-27][j], 0x000000);
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animMD[i-30][j], 0xffff00);

        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animSD[i-18][j], 0x000000);
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animSD[i-21][j], 0x0000ff);
      break;
        
      case 31:
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animMD[i-29][j], 0x000000);

        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animSD[i-20][j], 0x000000);
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animSD[i-23][j], 0x0000ff);
      break;

      case 32:
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animMD[i-31][j], 0x000000);

        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animSD[i-22][j], 0x000000);
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animSD[i-25][j], 0x0000ff);
      break;
        
      case 33:
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animMD[i-33][j], 0x000000);

        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animSD[i-24][j], 0x000000);
        for (int j = 0; j < 7; j++ ) strip.setPixelColor(animSD[i-27][j], 0x0000ff);
      break;

      case 34:
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animSD[i-26][j], 0x000000);
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animSD[i-29][j], 0x0000ff);
      break;

      case 35:
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animSD[i-28][j], 0x000000);
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animSD[i-31][j], 0x0000ff);
      break;

      case 36:
        for (int j = 0; j < 7; j++ ) strip.setPixelColor(animSD[i-30][j], 0x000000);
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animSD[i-33][j], 0x0000ff);
      break;

      case 37:
        for (int j = 0; j < 6; j++ ) strip.setPixelColor(animSD[i-32][j], 0x000000);
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animSD[i-35][j], 0x0000ff);
      break;

      case 38:
        for (int j = 0; j < 5; j++ ) strip.setPixelColor(animSD[i-34][j], 0x000000);
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animSD[i-37][j], 0x0000ff);
      break;

      case 39:
        for (int j = 0; j < 4; j++ ) strip.setPixelColor(animSD[i-36][j], 0x000000);
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animSD[i-39][j], 0x0000ff);
      break;

      case 40:
        for (int j = 0; j < 3; j++ ) strip.setPixelColor(animSD[i-38][j], 0x000000);
      break;

      case 41:
        for (int j = 0; j < 2; j++ ) strip.setPixelColor(animSD[i-40][j], 0x000000);
      break;

      case 42:
        for (int j = 0; j < 1; j++ ) strip.setPixelColor(animSD[i-42][j], 0x000000);
      break;
    }
    strip.show();                   
    delay(50);
  }

  //Set full color for each small pad
  for (int i = 0; i < 9; i++ ) strip.setPixelColor(padRD[i], 0xff0000);
  for (int i = 0; i < 9; i++ ) strip.setPixelColor(padLD[i], 0x00ff00);
  for (int i = 0; i < 9; i++ ) strip.setPixelColor(padRU[i], 0x0000ff);
  for (int i = 0; i < 9; i++ ) strip.setPixelColor(padLU[i], 0xffff00);
  
  strip.show();
  delay(200);
}

unsigned long ticks;
int tact;

void loop(){
  
   //Animation of white rhytm diods
   ticks = millis();
   strip.setPixelColor(rhytm[tact], 0x000000);
   tact = (ticks / 200) % 16;
   strip.setPixelColor(rhytm[tact], 0xffffff);

   //Rotation animation of small panels
   for (int i = 0; i < 8; i++ ) strip.setPixelColor(padRD[i], 0x000000);
   for (int i = 0; i < 8; i++ ) strip.setPixelColor(padLD[i], 0x000000);
   for (int i = 0; i < 8; i++ ) strip.setPixelColor(padRU[i], 0x000000);
   for (int i = 0; i < 8; i++ ) strip.setPixelColor(padLU[i], 0x000000);

   strip.setPixelColor(padLU[(ticks / 50) % 8], 0xffff00);
   strip.setPixelColor(padRU[(ticks / 50) % 8], 0x0000ff);
   strip.setPixelColor(padLD[(ticks / 50) % 8], 0x00ff00);
   strip.setPixelColor(padRD[(ticks / 50) % 8], 0xff0000);

   strip.setPixelColor(padLU[((ticks / 50) + 4 ) % 8 ], 0xffff00);
   strip.setPixelColor(padRU[((ticks / 50) + 4 ) % 8 ], 0x0000ff);
   strip.setPixelColor(padLD[((ticks / 50) + 4 ) % 8 ], 0x00ff00);
   strip.setPixelColor(padRD[((ticks / 50) + 4 ) % 8 ], 0xff0000);

    //read keystrokes and data transfer
    
   if (launchpad.getKeys()) {
        for (int i=0; i<10; i++)   // Scan the whole key list.
        {
            if ( launchpad.key[i].stateChanged )   // Only find keys that have changed state.
            {
                switch (launchpad.key[i].kstate) {  // Report active key state : IDLE, PRESSED, HOLD, or RELEASED
                    case PRESSED:
                    Serial.write(B10010010); //ON and Serial port which we use
                    if (launchpad.key[i].kchar < 59) {
                      strip.setPixelColor(launchpad.key[i].kchar -48, 0x0000ff);   
                      Serial.write(launchpad.key[i].kchar ); //Key of button
                    }
                    else if (launchpad.key[i].kchar < 97) {
                      strip.setPixelColor(launchpad.key[i].kchar -55, 0x0000ff);
                      Serial.write(launchpad.key[i].kchar -7); //Key of button
                    }
                    else {
                      strip.setPixelColor(launchpad.key[i].kchar -61, 0x0000ff);
                      Serial.write(launchpad.key[i].kchar -13); //Key of button
                    }
                    Serial.write(100);
                    
                break;
                    case HOLD:
                   
                break;
                    case RELEASED:
                    Serial.write(B10000010); //OFF and Serial port which we use
                    if (launchpad.key[i].kchar < 59) {
                      strip.setPixelColor(launchpad.key[i].kchar -48, 0x000000);     // залить синим 
                      Serial.write(launchpad.key[i].kchar ); //Key of button
                    }
                    else if (launchpad.key[i].kchar < 97) {
                       strip.setPixelColor(launchpad.key[i].kchar -55, 0x000000);
                      Serial.write(launchpad.key[i].kchar -7); //Key of button
                    }
                    else {
                      strip.setPixelColor(launchpad.key[i].kchar -61, 0x000000);
                      Serial.write(launchpad.key[i].kchar -13); //Key of button
                    }
                    Serial.write(100);
                break;
                
                    case IDLE:
 
                break;
                }
            
            }
        }
    }
    
    strip.show(); //Demonstration
}
